var express = require('express');
var router = express.Router();
var controladores = require('.././controladores');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/api/colaborativo', controladores.colaborativo.recColaborativo);
router.get('/api/contenido', controladores.contenido.recContenido);
module.exports = router;
