/**
 * Created by fernando on 16/05/17.
 */
var net = require('net');


module.exports = {

    //funciones del controlador

    recColaborativo: function (req, res, next) {
        var client = new net.Socket();
        //var elementos;
        client.connect(8000, '127.0.0.1', function () {
            console.log('Connected');
            var mascara = req.query.mascara;
            console.log("mascara "+mascara);
            client.write("colaborativo;"+mascara + '\n');
            console.log('datos enviados');
        });

        client.on('data', function (data) {
            //console.log("Datos de entrada ->"+ data);
            var elementos = data;
            client.destroy(); // kill client after server's response
            response(res,elementos);
        });

        client.on('close', function () {
            console.log('Connection closed');
        })

    }

}


function response(res,elementos)
{
     var json=JSON.parse(elementos);
    // var array = json.coincidencias;
    // var arrFinal = [];
    // for (var x = 0; x < array.length; x++) {
    //     arrFinal.push(busca(array, array[x]));
    // }
    //console.log(json)
    var arr=[]
    if(json.coincidencias.length==0){
        res.json({'recomendados': arr});
    }
    else{
    var datos = (json.coincidencias).split(";")
    for(var s=0;s<datos.length;s++){
        var ss=(datos[s].substring(1,((datos[s].length)-1))).split(',')
        //console.log({'elemento':ss[0], 'conteo':ss[1]})
        arr.push({'elemento':ss[0], 'conteo':ss[1]});
    }
    //console.log(datos)
    res.json({'recomendados': arr});
    }
}

/*function busca(arraybase,buscado)
{
    var count =0;
    for(var i=0;i<arraybase.length;i++){
        if(arraybase[i]==buscado){
            count++;
        }
    }

    return {'registro':buscado,'conteo':count}
}*/


